# Nano Ventex Cube

Desenvolvimento colaborativo de um ventilador mecânico emergencial de rápida manufatura baseado em **bombas compressoras e válvulas solenoides com confiabilidade certificada** para uso em equipamentos médicos. Nesta iniciativa os colaboradores do projeto Respire (Universidade de Brasília, IBF e Uniceplac) se juntaram às empresas Kohl Equipamentos e MN Smart Products (EUA) para o desenvolvimento de um ventilador mecânico baseado em bombas e válvulas certificadas no uso de equipamentos médicos.

## Colaboradores: ##
Equipe constituída por engenheiros, médicos e estudantes de engenharia.
* Universidade de Brasília 
* Instituto Federal de Brasília 
* Uniceplac
* Kohl Equipamentos 
* MN Smart Products, Minnesota - EUA

### Objetivo: ###
Construção de um protótipo funcional open source e de baixo custo de um ventilador mecânico. 

**Nota**: Este projeto não deve ser usado em animais ou humanos sem a prévia autorização de um comitê de ética em pesquisa.

### Facilidades: ###
Os Professores participantes do projeto têm experiência em:
* Projeto mecânico
* Projeto eletrônico
* Processamento de sinais

Os laboratórios disponíveis nas três instituições incluem:
* Simulação realística (manequins e software para configurar resistência pulmonar, monitoramento de parâmetros vitais)
* Impressoras 3D 
* Confecção de placas de circuito impresso e soldagem SMD 
* Plotter para corte a laser
* Tornos e fresadoras CNC para confecção de moldes 
* (Em fase de implantação): injeção de plástico para manufatura rápida

### Financiamento ###
Ajude-nos a financiar esta iniciativa
[(https://www.vakinha.com.br/vaquinha/nanoventex-cube-respirador-mecanico-de-baixo-custo-open-source)]

### Informações ###
* Ventilador pneumático baseado na disponibilidade de linhas de ar e oxigênio comprimido ou cilindros de oxigênio.
